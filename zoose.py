# Thunar custom actions launcher for the audio files
#  Milos Pavlovic 2015 <mpsrbija@gmail.com>
#
#  Save this file to /usr/local/bin/zoose.py
#  Setting thunar custom action:
#  Name: Audio options
#  Description : Manipulate Audio Files
#  Command: python3 /usr/local/bin/zoose.py "%F"
#  
#  File Pattern: *
#  Appearance: Directories, Audio files
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  

zoose_icons=[
["name","icon","command"],
["Audacious","audacious",'audacious'],
["Enqueue in Audacious","audacious",'audacious -e'],
["Clementine","clementine",'clementine -l'],
["Enqueue in Clementine","clementine",'clementine -a'],
["SMPlayer","smplayer",'smplayer'],
["Enqueue in  SMPlayer","smplayer",'smplayer -add-to-playlist'],
["VLC","vlc",'vlc'],
["Qmmp","qmmp",'qmmp'],
["Enqueue in Qmmp","qmmp",'qmmp -e'],
["Convert","soundconverter",'/usr/bin/soundconverter'],
["Bulk Rename (Thunar)","file-manager",'/usr/bin/thunar --bulk-rename'],
]


import os
import sys
import string
import subprocess
import shlex
from gi.repository import Gtk as Gtk
from gi.repository.GdkPixbuf import Pixbuf

# Which
def which(program):
        def is_exe(fpath):
            return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

        fpath, fname = os.path.split(program)
        if fpath:
            if is_exe(program):
                return program
        else:
            for path in os.environ["PATH"].split(os.pathsep):
                path = path.strip('"')
                exe_file = os.path.join(path, program)
                if is_exe(exe_file):
                    return exe_file
        return None

class Menu:

    def destroy(self, widget, data=None):
        # zoose mode
        Gtk.main_quit()

    def action(self, widget, event, x, data=None):
        if len(sys.argv) > 1 and  x != '':
           comm=" {0}".format(str(sys.argv[1]))
           command="{0}{1}". format(x, comm)
           subprocess.Popen(shlex.split(command), stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        else:
           print("No file(s) passed. Exiting.")
        Gtk.main_quit()

    def __init__(self):
        self.menu = Gtk.Menu()
        self.menu.connect("hide", self.destroy)
        it=Gtk.IconTheme.get_default()
        j=0
        first=True
        for line in zoose_icons:
            if first:
               first=False
               continue  
            try:
               if '/' in line[1]:
                  pixbuf=pixbuf_new_from_file(line[1])
               else:
                  pixbuf=it.load_icon(line[1],24,0)
            except:
               pixbuf=it.load_icon('gtk-stop',24,0)
            namen=(line[0])
            execc=line[2]

            chk = execc.split(' ')[0]
            if chk == "gksudo":
                    chk = execc.split(' ')[1]
            elif chk == "gksu":
                    chk = execc.split(' ')[1]
            elif chk == "kdesu":
                    chk = execc.split(' ')[1]

            x=execc
            checking = which(chk)
            if checking != None:
                box = Gtk.Box()
                box.set_spacing(10)
                img = Gtk.Image()
                img.set_from_pixbuf(pixbuf)
                label = Gtk.Label(namen)
                box.add(img)
                box.add(label)
                menuitem = Gtk.MenuItem()
                menuitem.add(box)
                menuitem.connect("button-press-event", self.action, x)
                self.menu.append(menuitem)
                j +=1
        height = j*30
        self.menu.set_size_request(180, height) # Workaround for height
        self.menu.popup(None, None, None, None, 0, Gtk.get_current_event_time())
        self.menu.show_all()
        print(height)
    def main(self):
        # Cliche init
        Gtk.main()

# I swear zoose this better work
if __name__ == "__main__":

     app = Menu()
     app.main()